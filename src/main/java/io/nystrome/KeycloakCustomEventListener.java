package io.nystrome;

import org.keycloak.events.Event;
import org.keycloak.events.EventListenerProvider;
import org.keycloak.events.admin.AdminEvent;

public class KeycloakCustomEventListener implements EventListenerProvider {

    @Override
    public void close() {        
    }

    @Override
    public void onEvent(Event event) {
        System.out.println("Event :" + event.getUserId());
        Producer.publishEvent(event.getType().toString(), event.toString());                
    }

    @Override
    public void onEvent(AdminEvent event, boolean includeRepresentation) {
        System.out.println("Admin Event:" + event.getResourceType().toString());
        Producer.publishEvent(event.getOperationType().toString(), event.getAuthDetails().toString());        
    }
    
}
